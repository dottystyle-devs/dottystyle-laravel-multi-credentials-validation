<?php

namespace Dottystyle\Laravel\MultiCredentialsValidation;

trait RegistersUserProvider
{
    /**
     * Registers the custom eloquent user provider.
     * 
     * @param string $name
     * @param bool $extend (optional)
     * @param callable $callback (optional)
     * @return void
     */
    public function registerEloquentProvider($name, $extend = false, $callback = null)
    {
        $this->app['auth']->provider($name, function ($app, $config) use ($extend, $callback) {
            $provider = new EloquentUserProvider($app['hash'], $config['model']);

            if ($extend) {
                $provider->extendWithOriginalValidation();
            }

            if ($callback) {
                call_user_func($callback, $provider);
            }

            return $provider;
        });
    }
}