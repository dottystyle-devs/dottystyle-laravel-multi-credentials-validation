<?php

namespace Dottystyle\Laravel\MultiCredentialsValidation;

trait CredentialsValidatorHelpers
{
    /**
     * @var string
     */
    protected $validatorLabel = '';

    /**
     * @var string
     */
    protected $validatorPriority = 0;

    /**
     * Set the label of the validator
     * 
     * @param string $label
     * @return self
     */
    public function setValidatorLabel($label)
    {
        $this->validatorLabel = $label;

        return $this;
    }
    
    /**
     * Get the label of the credentials validator.
     * 
     * @return string
     */
    public function getValidatorLabel()
    {
        return $this->validatorLabel;
    }

    /**
     * Set the priority of validator
     * 
     * @param int $priority
     * @return self
     */
    public function setValidatorPriority($priority)
    {
        $this->validatorPriority = $priority;

        return $this;
    }

    /**
     * Get the priority level of the validator
     * 
     * @return int
     */
    public function getValidatorPriority()
    {
        return $this->validatorPriority;
    }
}