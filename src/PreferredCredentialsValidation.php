<?php

namespace Dottystyle\Laravel\MultiCredentialsValidation;

interface PreferredCredentialsValidation
{
    /**
     * Get the preferred validation for the credentials.
     * 
     * @return string|CredentialsValidator
     */
    public function getPreferredCredentialsValidator();
}