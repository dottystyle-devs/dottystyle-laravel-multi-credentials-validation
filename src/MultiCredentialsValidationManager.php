<?php

namespace Dottystyle\Laravel\MultiCredentialsValidation;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Arr;
use InvalidArgumentException;

class MultiCredentialsValidationManager
{
    /**
     * @var array
     */
    protected $validators = [];

    /**
     * Validate credentials of a user using the specified validator.
     * 
     * @param string $validator
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @param  array  $credentials
     * @return bool
     * 
     * @throws InvalidArgumentException
     */
    public function validateUsing($validator, Authenticatable $user, array $credentials)
    {
        if (! $this->has($validator)) {
            throw new InvalidArgumentException(sprintf('Invalid credential validator %s', $validator));
        }

        return $this->get($validator)->validateCredentials($user, $credentials);
    }

    /**
     * Check whether there is an assigned credential validator with the given name.
     * 
     * @param string $validator
     * @return bool
     */
    public function has($validator)
    {
        return $this->findIndex($validator) !== -1;
    }

    /**
     * Get the validator config/details.
     * 
     * @param string $validator
     * @return \Dottystyle\Laravel\MultiCredentialsValidation\CredentialsValidator
     */
    public function get($validator)
    {
        $index = $this->findIndex($validator);

        return $index !== -1 ? $this->validators[$index] : null;
    }

    /**
     * Find the index of the specified validator from the validator list.
     * 
     * @param string $validator
     * @return int
     */
    protected function findIndex($validator)
    {
        foreach ($this->validators as $index => $_validator) {
            if ($_validator->getValidatorName() === $validator) {
                return $index;
            }
        }

        return -1;
    }

    /**
     * Add a credentials validator instance.
     * 
     * @param \Dottystyle\Laravel\MultiCredentialsValidation\CredentialsValidator $validator
     * @return self
     */
    public function extend(CredentialsValidator $validator)
    {
        $this->validators[] = $validator;
        
        // Sort by order
        $this->validators = Arr::sort($this->validators, function ($validator) {
            return $validator->getValidatorPriority();
        });

        return $this;
    }

    /**
     * Get all credential validators.
     * 
     * @return array
     */
    public function validators()
    {
        return $this->validators;
    }
}