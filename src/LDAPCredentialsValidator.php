<?php

namespace Dottystyle\Laravel\MultiCredentialsValidation;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Arr;
use ErrorException;

class LDAPCredentialsValidator implements CredentialsValidator
{
    use CredentialsValidatorHelpers;

    const PROTOCOL_VERSION = 3;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var string
     */
    protected $username;

    /**
     * @param array $config
     * @param string $config[host]
     * @param string $config[protocol_version]
     * @param string $config[domain]
     * @param string $config[rdn]
     */
    public function __construct(array $config, $username = 'username')
    {
        $this->config = Arr::only($config, ['host', 'protocol_version', 'domain', 'rdn']) + [
            'domain' => $config['host'],
            'protocol_version' => self::PROTOCOL_VERSION,
            'rdn' => ''
        ];

        $this->username = $username;
        $this->validatorLabel = 'LDAP';
    }

    /**
     * Get the name of the credentials validator.
     * 
     * @return string
     */
    public function getValidatorName()
    {
        return 'ldap';
    }

    /**
     * Attempt a validation credentials check using LDAP connection.
     * 
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        try {
            $connection = ldap_connect("ldap://{$this->config['host']}");
            ldap_set_option($connection, LDAP_OPT_PROTOCOL_VERSION, $this->config['protocol_version']);
            $bind = ldap_bind($connection, $this->prepareUsername($credentials[$this->username]), $credentials['password']);

            return true;
        } catch (ErrorException $e) {
            return false;
        }
    }

    /**
     * Prepare the username for authentication.
     * 
     * @param string $username
     * @return string
     */
    public function prepareUsername($username)
    {
        // Return the same if user already provided a DOMAIN\USERNAME format or the actual distinguished name.
        if (count(explode('\\', $username)) === 2 || strpos($username, 'cn=') === 0) {
            return $username;
        } 

        $parts = ["cn=$username"];
        
        if ($this->config['rdn']) {
            $parts[] = $this->config['rdn'];
        }
        
        $parts[] = 'dc='.str_replace('.', ',dc=', $this->config['domain']);

        return implode(',', $parts);
    }
}