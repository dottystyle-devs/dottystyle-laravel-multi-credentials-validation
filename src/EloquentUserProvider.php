<?php

namespace Dottystyle\Laravel\MultiCredentialsValidation;

use Illuminate\Auth\EloquentUserProvider as BaseProvider;

class EloquentUserProvider extends BaseProvider implements CredentialsValidator
{
    use ValidatesCredentialsMultiple, CredentialsValidatorHelpers;

    /**
     * @inheritdoc
     */
    public function getValidatorName()
    {
        return 'local';
    }

    /**
     * Get the validator label
     * 
     * @return string
     */
    public function getValidatorLabel()
    {
        return $this->validatorLabel ?: 'Local';
    }
}