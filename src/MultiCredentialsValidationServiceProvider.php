<?php

namespace Dottystyle\Laravel\MultiCredentialsValidation;

use Illuminate\Support\ServiceProvider;

class MultiCredentialsValidationServiceProvider extends ServiceProvider
{
    /**
     * Register services to the application
     * 
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MultiCredentialsValidationManager::class, function ($app) {
            return new MultiCredentialsValidationManager();
        });
    }

    /**
     * Get the services provided
     * 
     * @return array
     */
    public function provides()
    {
        return [MultiCredentialsValidationManager::class];
    }
}