<?php

namespace Dottystyle\Laravel\MultiCredentialsValidation;

use Illuminate\Contracts\Auth\Authenticatable;

interface CredentialsValidator
{
    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials);

    /**
     * Get the name of the credentials validator.
     * 
     * @return string
     */
    public function getValidatorName();

    /**
     * Get the label of the credentials validator.
     * 
     * @return string
     */
    public function getValidatorLabel();

    /**
     * Get the priority/order of the validator.
     * 
     * @return int
     */
    public function getValidatorPriority();
}