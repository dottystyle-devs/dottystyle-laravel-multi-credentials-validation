<?php

namespace Dottystyle\Laravel\MultiCredentialsValidation;

use Illuminate\Contracts\Auth\Authenticatable;

trait ValidatesCredentialsMultiple
{
    /**
     * Get the credentials validation manager instance
     * 
     * @return \App\MultiCredentialsValidationManager
     */
    public function getMultiCredentialsValidationManager()
    {
        return app(MultiCredentialsValidationManager::class);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array  $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $this->validateCredentialsCalled = true;
        $valid = false;

        
        // Try to validate the credentials using the preferred validation of the user.
        // Call the parent validate callback immediately if we are registered as an extension.
        // Validator can either be string or instance of CredentialsValidator
        if ($user instanceof PreferredCredentialsValidation) {
            if (is_string($validator = $user->getPreferredCredentialsValidator())) {
                if ($this->getMultiCredentialsValidationManager()->has($validator) &&
                $validator !== $this->getValidatorName()) {
                    $valid = $this->getMultiCredentialsValidationManager()->validateUsing($validator, $user, $credentials);
                } else {
                    $valid = parent::validateCredentials($user, $credentials);
                }
            } else {
                $valid = $validator->validateCredentials($user, $credentials);
            }
        } else {
            $valid = parent::validateCredentials($user, $credentials);
        }

        return $valid;
    }

    /**
     * Register the default credentials validation as extension.
     * 
     * @param string $method
     * @param array $options (optional)
     * @return self
     */
    public function extendWithOriginalValidation()
    {
        $this->getMultiCredentialsValidationManager()->extend($this);

        return $this;
    }
}
